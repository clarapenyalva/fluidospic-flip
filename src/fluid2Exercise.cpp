
#include "scene.h"
#include "pcg_solver.h"
#include <stdlib.h>

namespace
{
    //////////////////////////////////////////////
    // Add any custom classes or functions here //
    //////////////////////////////////////////////

	inline int clamp(int v, int lo, int hi) {
		v = std::min(hi, v);
		v = std::max(lo, v);
		return v;
	}

	//value, low, high
	inline float clampf(float v, float lo, float hi) {
		v = std::min(hi, v);
		v = std::max(lo, v);
		return v;
	}

	inline void clampInsideGrid(Vec2 &pos, const Vec2 & gridMinPos, const Vec2 & gridMaxPos)
	{
		float epsilon = 1e-5f;
		pos = Vec2(
			clampf(pos.x, gridMinPos.x + epsilon, gridMaxPos.x - epsilon),
			clampf(pos.y, gridMinPos.y + epsilon, gridMaxPos.y - epsilon)
		);
	}

	inline bool isInsideGrid(const Vec2 &pos, const Vec2 & gridMinPos, const Vec2 & gridMaxPos)
	{
		return pos.x >= gridMinPos.x &&	pos.y >= gridMinPos.y &&
			pos.x <= gridMaxPos.x && pos.y <= gridMaxPos.y;
	}
	
	inline float lerp(float v0, float v1, float t) { return v0 * (1 - t) + v1 * t; }

	inline float bilerp(float v00, float v10, float v01, float v11, float t, float s) {
		float v0 = lerp(v00, v10, t);
		float v1 = lerp(v01, v11, t);
		return lerp(v0, v1, s);
	}

	inline unsigned int indexInsideGrid(unsigned int pos, int min, int max)
	{
		return clamp((int)std::floor(pos), min, max);
	}

	void inverseBilerp(float value, const Vec2 &varIndex, Array2<float> &array, Array2<float> &arrayWeight)
	{
		//ix_jx --> i: x axis, j: y axis

		const Vec2 pos_i0_j0(floorf(varIndex.x), floorf(varIndex.y));
		const Vec2 pos_i1_j1(ceilf(varIndex.x), ceilf(varIndex.y));

		const Index2& size = array.getSize();

		unsigned int index_i0 = clamp((unsigned int)pos_i0_j0.x, 0, size.x - 1);
		unsigned int index_j0 = clamp((unsigned int)pos_i0_j0.y, 0, size.y - 1);
		unsigned int index_i1 = clamp((unsigned int)pos_i1_j1.x, 0, size.x - 1);
		unsigned int index_j1 = clamp((unsigned int)pos_i1_j1.y, 0, size.y - 1);

		const Index2 i0_j0(index_i0, index_j0);
		const Index2 i1_j0(index_i1, index_j0);
		const Index2 i0_j1(index_i0, index_j1);
		const Index2 i1_j1(index_i1, index_j1);

		float t = varIndex.x - (float)index_i0;
		float s = varIndex.y - (float)index_j0;

		array[i0_j0] += value * (1.0f - t) * (1.0f - s);
		array[i1_j0] += value * t * (1.0f - s);
		array[i0_j1] += value * (1.0f - t) * s ;
		array[i1_j1] += value * t * s;

		arrayWeight[i0_j0] += (1.0f - t) * (1.0f - s);
		arrayWeight[i1_j0] += t * (1.0f - s);
		arrayWeight[i0_j1] += (1.0f - t) * s;
		arrayWeight[i1_j1] += t * s;
	}

	float bilerpValueCellArray(const Array2<float> &arr, const Vec2 &indexPos)
	{
		//ix_jx --> i: x axis, j: y axis

		const Vec2 pos_i0_j0(floorf(indexPos.x), floorf(indexPos.y));
		const Vec2 pos_i1_j1(ceilf(indexPos.x), ceilf(indexPos.y));

		const Index2& size = arr.getSize();

		unsigned int index_i0 = clamp((unsigned int)pos_i0_j0.x, 0, size.x - 1);
		unsigned int index_j0 = clamp((unsigned int)pos_i0_j0.y, 0, size.y - 1);
		unsigned int index_i1 = clamp((unsigned int)pos_i1_j1.x, 0, size.x - 1);
		unsigned int index_j1 = clamp((unsigned int)pos_i1_j1.y, 0, size.y - 1);

		const Index2 i0_j0(index_i0, index_j0);
		const Index2 i1_j0(index_i1, index_j0);
		const Index2 i0_j1(index_i0, index_j1);
		const Index2 i1_j1(index_i1, index_j1);

		const Vec2 ts(indexPos - pos_i0_j0);

		const float value = bilerp(arr[i0_j0], arr[i1_j0 ], arr[i0_j1], arr[i1_j1],  ts.x, ts.y);
		return value;
	}

	float applyViscosity(Index2& pos, Index2& sizeFaces, Array2<float>& vel, const Vec2& invCellDx2, const float cteDtMuRho)
	{
		unsigned int i_0 =		clamp((unsigned int)pos.x, 0, sizeFaces.x - 1);
		unsigned int i_min1 =	clamp((unsigned int)pos.x - 1, 0, sizeFaces.x - 1);
		unsigned int i_1 =		clamp((unsigned int)pos.x + 1, 0, sizeFaces.x - 1);
		unsigned int j_0 =		clamp((unsigned int)pos.y, 0, sizeFaces.y - 1);
		unsigned int j_min1 =	clamp((unsigned int)pos.y - 1, 0, sizeFaces.y - 1);
		unsigned int j_1 =		clamp((unsigned int)pos.y + 1, 0, sizeFaces.y - 1);

		float vij = vel[Index2(i_0, j_0)];
		float vi1j = vel[Index2(i_1, j_0)];
		float vi_1j = vel[Index2(i_min1, j_0)];
		float vij1 = vel[Index2(i_0, j_1)];
		float vij_1 = vel[Index2(i_0, j_min1)];

		return vij + cteDtMuRho *
			(((vi_1j - 2.0f * vij + vi1j) * invCellDx2.x) +
			((vij_1 - 2.0f * vij + vij1) * invCellDx2.y));
	}

}

//init particles
void Fluid2::initParticles( void )
{
	// particle sampling on the entire domain
	Index2 sizeGrid = grid.getSize();
	Vec2 dxCell = grid.getCellDx();
	particles = Particles2(sizeGrid.x*sizeGrid.y*Scene::particlesPerCell);
	//float valueInk = 0.5f / Scene::particlesPerCell;
	float valueInk = 0.0f;

	for (int y = 0; y < sizeGrid.y; y++) //row
	{
		for (int x = 0; x < sizeGrid.x; x++) //col
		{
			Vec2 posCell = grid.getCellPos(Index2(x, y)) - Vec2(dxCell*0.5f);
			for (int i = 0; i < Scene::particlesPerCell; i++) //particles sampled per cell
			{
				float randX = ((float)rand()) / (float)RAND_MAX;
				float randY = ((float)rand()) / (float)RAND_MAX;

				float id =  y * sizeGrid.x * Scene::particlesPerCell + x * Scene::particlesPerCell + i;
				Vec2 posPart = Vec2(posCell.x + randX*dxCell.x, posCell.y + randY*dxCell.y);

				particles.setPosition(id, posPart);
				particles.setVelocity(id, Vec2());
				particles.setInk(id, valueInk);
			}
		}
	}
}

// advection
void Fluid2::fluidAdvection( const float dt )
{
    if( flipEnabled )
    {
		//get sizes 
		const Index2 sizeFacesX = grid.getSizeFacesX();
		const Index2 sizeFacesY = grid.getSizeFacesY();
		const Index2 sizeGrid = grid.getSize();

		Bbox2 gridDomain = grid.getDomain();
		Vec2 gridMinPos = gridDomain.minPosition;
		Vec2 gridMaxPos = gridDomain.maxPosition;

		//auxiliar arrays of properties
		Array2<float> inkAux;
		Array2<float> velocityXAux;
		Array2<float> velocityYAux;

		inkAux.resize(ink.getSize());
		velocityXAux.resize(velocityX.getSize());
		velocityYAux.resize(velocityY.getSize());
		
		//auxiliar arrays that contains the weights
		Array2<float> inkWeights;
		Array2<float> velocityXWeights;
		Array2<float> velocityYWeights;

		inkWeights.resize(ink.getSize());
		velocityXWeights.resize(velocityX.getSize());
		velocityYWeights.resize(velocityY.getSize());

		Vec2 vel;
		Vec2 pos;

        // move particles with RK2 with grid velocities
		for (int i = 0; i < particles.getSize(); i++)
		{			
			Vec2 pos0 = particles.getPosition(i);
	
			//1 - asignar a la partícula la vel. correspodiente a la proporción del grid
			//2 - intergrar la posición de las partículas con RK2

			//Integrate to h/2
			Vec2 faceIndexX = grid.getFaceIndex(pos0, 0);
			Vec2 faceIndexY = grid.getFaceIndex(pos0, 1);			
			float velX = bilerpValueCellArray(velocityX, faceIndexX);
			float velY = bilerpValueCellArray(velocityY, faceIndexY);
			Vec2 vel = Vec2(velX, velY);

			pos = pos0 + (0.5f * dt) * vel;

			faceIndexX = grid.getFaceIndex(pos, 0);
			faceIndexY = grid.getFaceIndex(pos, 1);
			velX = bilerpValueCellArray(velocityX, faceIndexX);
			velY = bilerpValueCellArray(velocityY, faceIndexY);
			Vec2 velMid = Vec2(velX, velY);

			//Integrate to h
			pos = pos0 + dt * velMid;

			// ensure particle remains inside the domain
			clampInsideGrid(pos, gridMinPos, gridMaxPos);

			particles.setPosition(i, pos);
			
			// compute all the properties
			inverseBilerp(particles.getInk(i), grid.getCellIndex(pos), 
				inkAux, inkWeights);
			inverseBilerp(particles.getVelocity(i).x, grid.getFaceIndex(pos, 0), 
				velocityXAux, velocityXWeights);
			inverseBilerp(particles.getVelocity(i).y, grid.getFaceIndex(pos, 1), 
				velocityYAux, velocityYWeights);
		}

		//actualize grid properties

		//ink
		for (int y = 0; y < ink.getSize().y; y++)
			for (int x = 0; x < ink.getSize().x; x++)
			{
				Index2 id(x, y);
				inkWeights[id] < 1e-5 ? ink[id] = 0.0f : ink[id] = inkAux[id] / inkWeights[id];
			}

		//velocityX
		for (int y = 0; y < velocityX.getSize().y; y++)
			for (int x = 0; x < velocityX.getSize().x; x++)
			{
				Index2 id(x, y);
				velocityXWeights[id] < 1e-5 ? velocityX[id] = 0.0f : velocityX[id] = velocityXAux[id] / velocityXWeights[id];

			}

		//velocityY
		for (int y = 0; y < velocityY.getSize().y; y++)
			for (int x = 0; x < velocityY.getSize().x; x++)
			{
				Index2 id(x, y);
				velocityYWeights[id] < 1e-5 ? velocityY[id] = 0.0f : velocityY[id] = velocityYAux[id] / velocityYWeights[id];

			}

		// save current state velocities
		oldVelocityX = velocityX;
		oldVelocityY = velocityY;
    }

	else
	{
		//get sizes 
		const Index2 sizeFacesX = grid.getSizeFacesX();
		const Index2 sizeFacesY = grid.getSizeFacesY();
		const Index2 sizeGrid = grid.getSize(); // = ink size

		Bbox2 gridDomain = grid.getDomain();
		Vec2 gridMinPos = gridDomain.minPosition;
		Vec2 gridMaxPos = gridDomain.maxPosition;

		//auxiliar arrays of properties
		Array2<float> velocityXAux(velocityX);
		Array2<float> velocityYAux(velocityY);
		Array2<float> inkAux(ink);


		//ink
		for (unsigned int x = 0; x < sizeGrid.x; x++)
			for (unsigned int y = 0; y < sizeGrid.y; y++)
			{
				Index2 index(x, y);

				Vec2 currPosition(grid.getCellPos(index));
				Vec2 velocity(	(velocityX[index] + velocityX[Index2(x + 1, y)]) * 0.5f,
								(velocityY[index] + velocityY[Index2(x, y + 1)]) * 0.5f);
		
				//Backward interpolation x(i-1) = x(i) + dt * -v(i)
				Vec2 prevPosition = currPosition - dt * velocity;
				
				Vec2 cellIndex = grid.getCellIndex(prevPosition);
				ink[index] = bilerpValueCellArray(inkAux, cellIndex);
			}

		//velocity
		for (unsigned int x = 0; x < sizeFacesX.x; x++)
			for (unsigned int y = 0; y < sizeFacesX.y; y++)
			{
				//Position in grid
				Index2 pos(x, y);

				const Index2 index(x, y);
				
				//Position x(i)
				const Vec2 currPosition(grid.getFaceXPos(index));

				//Velocity v(i)
				float u = velocityXAux[index];

				//velocity Y must be the average between the 4 v closer to u
				unsigned int i_lerp0 = indexInsideGrid(pos.x, 0, sizeFacesY.x - 1);
				unsigned int i_lerp1 = indexInsideGrid(pos.x - 1, 0, sizeFacesY.x - 1);
				unsigned int j_lerp0 = indexInsideGrid(pos.y, 0, sizeFacesY.y - 1);
				unsigned int j_lerp1 = indexInsideGrid(pos.y - 1, 0, sizeFacesY.y - 1);

				float v = bilerp(
					velocityYAux[Index2(i_lerp0, j_lerp0)],
					velocityYAux[Index2(i_lerp1, j_lerp0)],
					velocityYAux[Index2(i_lerp0, j_lerp1)],
					velocityYAux[Index2(i_lerp1, j_lerp1)],
					0.5f, 0.5f);

				Vec2 velocity(u, v);

				//Backward interpolation x(i-1) = x(i) + dt * -v(i)
				const Vec2 prevPosition(currPosition - dt * velocity);
				Vec2 faceIndexX = grid.getFaceIndex(prevPosition, 0);
				velocityX[index] = bilerpValueCellArray(velocityXAux, faceIndexX);
			}

		for (unsigned int x = 0; x < sizeFacesY.x; x++)
			for (unsigned int y = 0; y < sizeFacesY.y; y++)
			{
				//Position in grid
				Index2 pos(x, y);

				const Index2 index(x, y);

				//Position x(i)
				const Vec2 currPosition(grid.getFaceYPos(index));

				//Velocity v(i)
				float v = velocityYAux[index];

				//velocity Y must be the average between the 4 v closer to u
				unsigned int i_lerp0 = indexInsideGrid(pos.x, 0, sizeFacesX.x - 1);
				unsigned int i_lerp1 = indexInsideGrid(pos.x - 1, 0, sizeFacesX.x - 1);
				unsigned int j_lerp0 = indexInsideGrid(pos.y, 0, sizeFacesX.y - 1);
				unsigned int j_lerp1 = indexInsideGrid(pos.y - 1, 0, sizeFacesX.y - 1);

				float u = bilerp(
					velocityXAux[Index2(i_lerp0, j_lerp0)],
					velocityXAux[Index2(i_lerp1, j_lerp0)],
					velocityXAux[Index2(i_lerp0, j_lerp1)],
					velocityXAux[Index2(i_lerp1, j_lerp1)],
					0.5f, 0.5f);

				Vec2 velocity(u, v);

				//Backward interpolation x(i-1) = x(i) + dt * -v(i)
				const Vec2 prevPosition(currPosition - dt * velocity);
				Vec2 faceIndexY = grid.getFaceIndex(prevPosition, 1);
				velocityY[index] = bilerpValueCellArray(velocityYAux, faceIndexY);
			}
	}
}

// emission
void Fluid2::fluidEmission( void )
{
	const float valueInk = 1.0f;
	//To visualize standart position, numEmitters = 1
	const unsigned int numEmitters = 3;
	Bbox2 emitters[numEmitters];
	//in global coordinates
	emitters[0] = Bbox2(-0.15f, -1.5f, 0.15f, -1.3f);
	emitters[1] = Bbox2(-1.7f, 1.8f, -1.6f, 1.9f);
	emitters[2] = Bbox2(1.6f, -1.9f, 1.7f, -1.8f);

	Vec2 iniVel[numEmitters];
	iniVel[0] = Vec2(0.0f, 4.0f);
	iniVel[1] = Vec2(2.0f, -5.0f);
	iniVel[2] = Vec2(-2.0f, 5.0f);

    if( flipEnabled )
    {
        // modify particles properties if inside the emitters
		for (int i = 0; i < particles.getSize(); i++) 
		{
			for (int j = 0; j < numEmitters; j++)
			{
				Vec2 pos = particles.getPosition(i);
				if (isInsideGrid(pos, emitters[j].minPosition, emitters[j].maxPosition))
				{
					particles.setInk(i, valueInk);
					particles.setVelocity(i, iniVel[j]);
				}
			}
		}
    }
	else
	{
		//To grid coordinates
		for (int j = 0; j < numEmitters; j++)
		{
			Vec2 posMinV = grid.getCellIndex(emitters[j].minPosition);
			Vec2 posMaxV = grid.getCellIndex(emitters[j].maxPosition);

			Index2 posMin((int)floor(posMinV.x), (int)floor(posMinV.y));
			Index2 posMax((int)floor(posMaxV.x), (int)floor(posMaxV.y));

			for (unsigned int x = posMin.x; x <= posMax.x; ++x)
				for (unsigned int y = posMin.y; y <= posMax.y; ++y)
				{
					ink[Index2(x, y)] = valueInk;
					velocityX[Index2(x, y)] = iniVel[j].x;
					velocityY[Index2(x, y)] = iniVel[j].y;
				}
		}
	}
}

// volume forces
void Fluid2::fluidVolumeForces( const float dt )
{
    // grid-based volume forces
	// gravity -> update velocityY
	const Index2 sizeFacesY = grid.getSizeFacesY();
	for (unsigned int y = 0; y < sizeFacesY.y; y++)
		for (unsigned int x = 0; x < sizeFacesY.x; x++)
		{
			Index2 pos(x, y);
			velocityY[pos] += dt * Scene::kGravity;
		}    
}

// viscosity
void Fluid2::fluidViscosity( const float dt )
{
    // grid-based viscosity
	//sizes
	Index2 sizeFacesX = grid.getSizeFacesX();
	Index2 sizeFacesY = grid.getSizeFacesY();

	//copy vectors of velocities
	Array2<float> copyVelX(velocityX);
	Array2<float> copyVelY(velocityY);

	//constant to calculate viscosity
	const Vec2 cellDx = grid.getCellDx();
	const Vec2 invCellDx2(1.0f / (cellDx.x*cellDx.x), 1.0f / (cellDx.y*cellDx.y));
	const float cteDtMuRho = (dt / Scene::kDensity) * Scene::kViscosity;
	
	//VELOCITY X --------------------------------------------------
	for (unsigned int x = 0; x < sizeFacesX.x; x++)//column
		for (unsigned int y = 0; y < sizeFacesX.y; y++)//row
		{
			Index2 pos(x, y);
			velocityX[pos] = applyViscosity(pos, sizeFacesX, copyVelX, invCellDx2, cteDtMuRho);
		}
	//END VELOCITY X --------------------------------------------------

	//VELOCITY Y --------------------------------------------------
	for (unsigned int x = 0; x < sizeFacesY.x; x++)//column
		for (unsigned int y = 0; y < sizeFacesY.y; y++)//row
		{
			Index2 pos(x, y);
			velocityY[pos] = applyViscosity(pos, sizeFacesY, copyVelY, invCellDx2, cteDtMuRho);
		}
	//END VELOCITY Y --------------------------------------------------
}

void Fluid2::fluidPressureProjection(const float dt)
{
	// grid-based pressure projection
	Index2 gridSize = grid.getSize(); // = pressure size
	Index2 sizeFacesX = grid.getSizeFacesX(); // = velX size
	Index2 sizeFacesY = grid.getSizeFacesY(); // = velY size

	const Vec2 cellDx = grid.getCellDx();
	const Vec2 invCellDx(1.0f / cellDx.x, 1.0f / cellDx.y);
	const Vec2 invCellDx2(1.0f / (cellDx.x*cellDx.x), 1.0f / (cellDx.y*cellDx.y));
	const float cteRhoDt = Scene::kDensity / dt;
	const float cteDtRho = dt / Scene::kDensity;

	//Solver parameters
	PCGSolver<double> solver;
	solver.set_solver_parameters(1e-6, 10000);
	double residual;
	int it;
	SparseMatrix<double> A(gridSize.x * gridSize.y, 5);
	std::vector<double> b(gridSize.x * gridSize.y);
	std::vector<double> res(gridSize.x * gridSize.y);

	Array2<float> newVelX(sizeFacesX);
	Array2<float> newVelY(sizeFacesY);

	//boundary conditions
	for (unsigned int y = 0; y < gridSize.y; y++)
	{
		velocityX[Index2(0, y)] = 0.0f;
		velocityX[Index2(sizeFacesX.x - 1, y)] = 0.0f;
	}
	for (unsigned int x = 0; x < gridSize.x; x++)
	{
		velocityY[Index2(x, 0)] = 0.0f;
		velocityY[Index2(x, sizeFacesY.y - 1)] = 0.0f;
	}

	//Solver of pressure-----------------------------------------------------------
	//right hand side = b
	for (unsigned int x = 0; x < gridSize.x; x++)//column
		for (unsigned int y = 0; y < gridSize.y; y++) //row
		{
			const Index2 index(x, y);
			b[pressure.getLinearIndex(x, y)] = -cteRhoDt *
				((velocityX[Index2(x + 1, y)] - velocityX[index]) * invCellDx.x +
				(velocityY[Index2(x, y + 1)] - velocityY[index]) * invCellDx.y);
		}

	//create A
	for (unsigned int x = 0; x < gridSize.x; x++)//column
		for (unsigned int y = 0; y < gridSize.y; y++) //row
		{
			const unsigned int index = pressure.getLinearIndex(x, y);	
			unsigned int idAux;
			A.add_to_element(index, index, 1e-6);

			if (x > 0) {
				idAux  = pressure.getLinearIndex(x - 1, y);
				A.add_to_element(index, index, 1.0f * invCellDx2.x);
				A.add_to_element(index, idAux, -1.0f * invCellDx2.x);
			}
			if (x < gridSize.x - 1) {
				idAux = pressure.getLinearIndex(x + 1, y);
				A.add_to_element(index, index, 1.0f * invCellDx2.x);
				A.add_to_element(index, idAux, -1.0f * invCellDx2.x);
			}
			if (y > 0) {
				idAux = pressure.getLinearIndex(x, y - 1);
				A.add_to_element(index, index, 1.0f * invCellDx2.y);
				A.add_to_element(index, idAux, -1.0f * invCellDx2.y);
			}
			if (y < gridSize.y - 1) {
				idAux = pressure.getLinearIndex(x, y + 1);
				A.add_to_element(index, index, 1.0f * invCellDx2.y);
				A.add_to_element(index, idAux, -1.0f * invCellDx2.y);
			}
		}

	//Solve system
	solver.solve(A, b, res, residual, it);
	//std::cout << "res = " << residual << std::endl << "it = " << it << std::endl;
	//End solver of pressure ------------------------------------------------------

	//Update pressure -------------------------------------------------------------
	for (unsigned int x = 0; x < gridSize.x; x++)
		for (unsigned int y = 0; y < gridSize.y; y++)
		{
			pressure[Index2(x, y)] = res[pressure.getLinearIndex(x, y)];
		}
	//End update pressure ---------------------------------------------------------

	//Velocities ------------------------------------------------------------------
	for (unsigned int x = 1; x < sizeFacesX.x - 1; x++)
		for (unsigned int y = 0; y < sizeFacesX.y; y++)
		{
			const Index2 index(x, y);
			velocityX[index] -= cteDtRho * (pressure[index] - pressure[Index2(x - 1, y)]) * invCellDx.x;
			
		}

	for (unsigned int x = 0; x < sizeFacesY.x; x++)
		for (unsigned int y = 1; y < sizeFacesY.y - 1; y++)
		{
			const Index2 index(x, y);
			velocityY[index] -= cteDtRho * (pressure[index] - pressure[Index2(x, y - 1)]) * invCellDx.y;
		}
	//End velocities --------------------------------------------------------------

	if (flipEnabled)
	{
		// calculate FLIP velocity delta
		Array2<float> deltaVelocityX(grid.getSizeFacesX().x, grid.getSizeFacesX().y);
		Array2<float> deltaVelocityY(grid.getSizeFacesY().x, grid.getSizeFacesY().y);

		for (int x = 0; x < sizeFacesX.x; x++)
			for (int y = 0; y < sizeFacesX.y; y++)
				deltaVelocityX[Index2(x, y)] = velocityX[Index2(x, y)] - oldVelocityX[Index2(x, y)];

		for (int x = 0; x < sizeFacesY.x; x++)
			for (int y = 0; y < sizeFacesY.y; y++)
				deltaVelocityY[Index2(x, y)] = velocityY[Index2(x, y)] - oldVelocityY[Index2(x, y)];

		// apply PIC/FLIP to update particles velocities
		for (int i = 0; i < particles.getSize(); i++) {
			Vec2 faceIndexX = grid.getFaceIndex(particles.getPosition(i), 0);
			Vec2 faceIndexY = grid.getFaceIndex(particles.getPosition(i), 1);

			Vec2 velPIC = Vec2(bilerpValueCellArray(velocityX, faceIndexX),
				bilerpValueCellArray(velocityY, faceIndexY));

			Vec2 velFLIP = Vec2(bilerpValueCellArray(deltaVelocityX, faceIndexX),
				bilerpValueCellArray(deltaVelocityY, faceIndexY));

			particles.setVelocity(i, 0.95f * (particles.getVelocity(i) + velFLIP) + 0.05f * velPIC);
		}
	}
}
